require(['vue','ELEMENT','base','vue-resource','Message'],function(Vue,ELEMENT,baseUtil,VueResource,messageHelper){
	Vue.use(VueResource);
	Vue.use(ELEMENT);
	Vue.use(messageHelper);
	var approval = new Vue({
		el:"#approvalList",
		data:{
			tableData:[],
			getURL:baseUtil.WebRootUrl+"/LeaveBillFlowControl/findByCurrentTask",
			completeURL:baseUtil.WebRootUrl+"/LeaveBillFlowControl/completeTask",
		},
		mounted:function(){
			this.getData();
		},
		methods:{
			getData(){
				var vm = this;
				vm.$http.get(vm.getURL)
				.then((response)=>{
					if(response.data.result == "200"){
    					vm.tableData = response.data.dataList;
    					console.log(response.data.dataList);
    				}else{
		    			 console.log(response);
		    			 vm.$showMess({message:'出异常了!',messType:'error'});
		    		 }
				})
				.catch((response)=>{
					vm.$showMess({message:'出异常了!',messType:'error'});
				});
			},
			completeTask(row,msg){
				var tid = row.taskId;
				var vm = this;
				vm.$http.post(vm.completeURL,{"taskId":tid,"outcome":msg},{emulateJSON:true})
				.then((response)=>{
					if(response.data.result == "200"){
    					vm.getData();
    				}else{
		    			 console.log(response);
		    			 vm.$showMess({message:'出异常了!',messType:'error'});
		    		 }
				})
				.catch((response)=>{
					vm.$showMess({message:'出异常了!',messType:'error'});
				});
			},
			dateFormat(row,column){
    			var date = row[column.property];
				if(date == null){
					return "";
				}else{
					return new Date(date).format("yyyy-MM-DD HH:mm:ss");
				}
		   }
		}
	});
});