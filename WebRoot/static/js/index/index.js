define(['vue', 'velocity', 'vue-resource', 'ELEMENT', 'base', 'Scroll','Message'], function(Vue, Velocity, VueResource, ELEMENT, baseUtil, Scroll,messageHelper) {
    //安装 vue的ajax功能
    Vue.use(VueResource);
    Vue.use(ELEMENT);
    Vue.use(Scroll);
    //安装message
    Vue.use(messageHelper);
    var indexModule = {
        IndexVues: {},
        //各个模块的vm配置参数
        init: function() {
            document.title = '系统登录中。。。';
            document.getElementById('showWelcome').style.display = 'block';
            var WelcomeOption = {
                el: '#showWelcome',
                data: {
                    duration: '5000',
                    show: false,
                    items: "欢迎来到本系统。作者:YLG/WY。系统正在加载中。请稍后。。。".split("").map(function(item, index, array) {
                        return {
                            msg: item,
                            show: false
                        }
                    }),
                    stop: false,
                    canEnter: true,
                    canLeave: false,
                    systemAnimate: ['slideUp', 'slideDown', 'fadeIn', 'fadeOut']

                },
                mounted: function() {

                    this.items.forEach(function(item) {
                        item.show = true;
                    });
                },
                methods: {
                    enter: function(el, done) {
                        var delay = el.dataset.index * 150;
                        var vm = this;
                        if (vm.canEnter && !vm.stop) {
                            setTimeout(function() {
                                Velocity(el, {
                                    opacity: 1,
                                    height: '1.6em'
                                }, {
                                    complete: function() {
                                        //vm.items[el.dataset.index].show = false;
                                        var index = el.dataset.index;
                                        var length = vm.items.length;
                                        if (index == length - 1) {
                                            setTimeout(function() {
                                                vm.canEnter = false;
                                                vm.canLeave = true;
                                                vm.items.forEach(function(item, index, array) {
                                                    item.show = false;
                                                });

                                            }, 2000);

                                        }

                                        done();
                                    }
                                })
                            }, delay);
                        }
                    },
                    beforeEnter: function(el) {
                        el.style.opacity = 0;
                        el.style.height = 0;
                    },
                    leave: function(el, done) {
                        var delay = el.dataset.index * 150;
                        var vm = this;
                        if (vm.canLeave) {
                            /* setTimeout(function () {
					        Velocity(
					          el,
					          { opacity: 0, height: 0 },
					          { complete:function(){ 
					          	//vm.items[el.dataset.index].show = true;
					        	 
					        	 
					          	done();
					          }
					          }
					        )
					      }, delay);*/
                            var index = el.dataset.index;
                            var length = vm.items.length;
                            if (index == length - 1 && !vm.stop) {
                                setTimeout(function() {
                                    vm.items.forEach(function(item, index, array) {
                                        item.show = true;
                                    });
                                    vm.canEnter = true;
                                    vm.canLeave = false;
                                }, 0);

                            }
                        }
                    }
                }
            }
            var vm = this.getVue(WelcomeOption);
            indexModule.IndexVues['headVm'] = vm;
            //设置5秒后关闭系统欢迎界面。
            setTimeout(function() {
                vm.stop = true;
                var e1 = document.getElementById('showWelcome');
                var e2 = document.getElementById('container');
                Velocity(e1, vm.systemAnimate[3], // fadeOut
                {
                    loop: 0,
                    easing: "easeInSine",

                    complete: function() {
                        //	e1.style.display = 'none';
                        //	e2.style.display = 'block';
                        Velocity(e2, vm.systemAnimate[1], // slideDown
                        {

                            easing: "easeInSine",
                            complete: function() {
                                //	e1.style.display = 'none';
                                //	e2.style.display = 'block';
                                document.title = '欢迎来到BMS管理系统';
                                indexModule.initLeftMenu();
                                indexModule.initHead();
                            }
                        });
                    }
                });

                //document.getElementById('showWelcome').style.display = 'none';
                //document.getElementById('container').style.display = 'block';
            }, 0);

        },
        getVue: function(option) {
            var vue = new Vue(option);
            return vue;
        },
        defaultUrl: "controduce/index.html",
        initHead: function() {
            var option = {
                el: '#head',
                data: {
                    bmstabs: [{
                        url: "#",
                        menuname: "首页",
                        name: "index",
                        closable: false
                    }],
                    bmsactivetab: "#",
                    logoutURL: baseUtil.WebRootUrl + "/LoginControl/logOut"
                },
                mounted: function() {
                    document.getElementById("indexFrame").src = indexModule.defaultUrl;
                },
                methods: {
                    tabClick: function() {
                        console.log(arguments);
                        console.log(indexModule.IndexVues['initHead'].bmsactivetab);
                        var url = indexModule.IndexVues['initHead'].bmsactivetab;
                        if (url == '#') {
                            url = indexModule.defaultUrl;
                        }
                        document.getElementById("indexFrame").src = url;
                    },
                    removeTab: function(targetName) {
                        var tabs = this.bmstabs;
                        var activeName = this.bmsactivetab;
                        if (activeName === targetName) {
                            tabs.forEach((tab,index)=>{
                                if (tab.url === targetName) {
                                    var nextTab = tabs[index + 1] || tabs[index - 1];
                                    if (nextTab) {
                                        activeName = nextTab.url;
                                    }
                                }
                            }
                            );
                        }

                        this.bmsactivetab = activeName;
                        this.bmstabs = tabs.filter(tab=>tab.url !== targetName);
                        var url = indexModule.IndexVues['initHead'].bmsactivetab;
                        if (url == '#') {
                            url = indexModule.defaultUrl;
                        }
                        document.getElementById("indexFrame").src = url;
                    },
                    handerOpsClick:function(command){
                    	if(command == 'logout'){
                    		var vm = this;
                    		vm.$http.get(vm.logoutURL).then((response)=>{
                            	vm.info = response.data.info;
                                if (response.data.result == "200") {
                                    // console.log(response.data.dataList);
                                	vm.$showMess({message:'成功登出',messType:'success'});
                                	window.location.href="login.html";
                                }
                            }
                            ).catch(function(response) {
                            	vm.$showMess({message:'登出失败',messType:'error'});
                            });
                    		
                    	}
                    }
                }

            }
            var vm = this.getVue(option);
            indexModule.IndexVues['initHead'] = vm;
            return vm;
        },
        initLeftMenu: function() {

            var option = {

                data: {
                    menus: [],
                    info: "",
                    getURL: baseUtil.WebRootUrl + "/SysMenuControl/getcurrentUserMenu",

                },

                mounted: function() {
                    this.getData();
                },
                methods: {
                    handleSelect: function() {
                        var vueComponets = arguments[2];
                        var url = vueComponets.$el.id;
                        var menuname = vueComponets.$el.getAttribute("name");
                        document.getElementById("indexFrame").src = url;
                        var f = indexModule.IndexVues['initHead'].bmstabs.every(function(item) {
                            return item.url !== url;
                        });
                        if (f) {
                            indexModule.IndexVues['initHead'].bmstabs.push({
                                name: url,
                                menuname: menuname,
                                url: url,
                                closable: true
                            });
                        }

                        indexModule.IndexVues['initHead'].bmsactivetab = url;

                    },
                    handleOpen: function(key, keyPath) {
                        console.log(key, keyPath);
                    },
                    handleClose: function(key, keyPath) {
                        console.log(key, keyPath);
                    },
                    getData: function() {
                        var LeftMenuVm = this;
                        LeftMenuVm.$http.get(LeftMenuVm.getURL, {
                            "data.pid": "-1"
                        }).then((response)=>{
                        	var menuArray = [];
                        	for(var i in response.data.dataList){
                        		 
                        		 menuArray.push(response.data.dataList[i]);
                        		 
                        	}
                        	 
                        		
                            LeftMenuVm.info = response.data.info;
                            if (response.data.result == "200") {
                                // console.log(response.data.dataList);

                                //vm.$set(LeftMenuVm, 'menus', response.data.dataList);
                            	vm.$set(LeftMenuVm, 'menus', menuArray);
                            }
                        }
                        ).catch(function(response) {
                            alert("获取菜单失败！");
                        });

                    }
                },
                components: {

                    'bmsmenu': {
                        name: "bmsmenu",
                        template: '#left-menu-template',
                        props: ['menu', 'index', 'allindex'],
                        data: function() {
                            return {
                                WebRootUrl: baseUtil.WebRootUrl + "/"
                            }
                        },
                        computed: {
                            getIndex: function() {
                                return this.allindex.trim().toLowerCase();
                            }
                        }
                    }

                }
            }
            var vm = new Vue(option).$mount('#leftmenu');
            indexModule.IndexVues['leftMenu'] = vm;
            return vm;
        }
    }
    return indexModule;
});