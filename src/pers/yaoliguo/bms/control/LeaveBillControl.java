package pers.yaoliguo.bms.control;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import pers.yaoliguo.bms.common.PagerBean;
import pers.yaoliguo.bms.control.view.Message;
import pers.yaoliguo.bms.entity.SysUser;
import pers.yaoliguo.bms.entity.leaveBill;
import pers.yaoliguo.bms.service.ILeaveBillService;
import pers.yaoliguo.bms.uitl.StringHelper;

/**
 * @ClassName:       LeaveBillControl
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年8月16日        下午9:43:39
 */
@Controller
@RequestMapping("/LeaveBillControl")
public class LeaveBillControl extends BaseControl{
	
	@Autowired
	ILeaveBillService leaveBillService;
	
	@RequestMapping("/skipLeaveBillListPage")
	public String skipLeaveBillListPage(){
		
		return "redirect:/views/leaveBill/LeaveBeillList.html";
	}
	
	@ResponseBody
	@RequestMapping("/getLeaveBillList")
	public Object getLeaveBillList(PagerBean page,leaveBill bill){
		Message<leaveBill> msg = new Message<leaveBill>();
		List<leaveBill> list = new ArrayList<leaveBill>();
		try {
			list = leaveBillService.selectAll(bill, page);
			msg.setDataList(list);
			msg.setResult("200");
			msg.setPage(page);
		} catch (Exception e) {
			msg.setResult("500");
		}
		return msg;
	}
	
	@ResponseBody
	@RequestMapping("/addLeaveBill")
	public Object addLeaveBill(leaveBill bill){
		Message<leaveBill> msg = new Message<leaveBill>();
		try {
			SysUser user = getLoginUser();
			bill.setId(StringHelper.getUUID());
			bill.setLeveDate(new Date());
			bill.setUserId(user.getId());
			bill.setState(0);
			leaveBillService.insert(bill);
			msg.setResult("200");
		} catch (Exception e) {
			msg.setResult("500");
		}
		return msg;
	}
	
	@ResponseBody
	@RequestMapping("/deleteLeaveBill")
	public Object deleteLeaveBill(leaveBill bill){
		Message<leaveBill> msg = new Message<leaveBill>();
		try {
			leaveBillService.deleteByPrimaryKey(bill.getId());
			msg.setResult("200");
		} catch (Exception e) {
			msg.setResult("500");
		}
		return msg;
	}
	
	@ResponseBody
	@RequestMapping("/updateLeaveBill")
	public Object updateLeaveBill(leaveBill bill){
		Message<leaveBill> msg = new Message<leaveBill>();
		try {
			leaveBillService.updateByPrimaryKeySelective(bill);
			msg.setResult("200");
		} catch (Exception e) {
			msg.setResult("500");
		}
		return msg;
	}
	
	@ResponseBody
	@RequestMapping("/getLeaveBillById")
	public Object getLeaveBillById(leaveBill bill){
		Message<leaveBill> msg = new Message<leaveBill>();
		try {
			bill = leaveBillService.selectByPrimaryKey(bill.getId());
			msg.setResult("200");
			msg.setData(bill);
		} catch (Exception e) {
			msg.setResult("500");
		}
		return msg;
	}
	

}
