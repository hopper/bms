package pers.yaoliguo.bms.uitl;

import java.security.MessageDigest;

/**
 * @ClassName:       MD5Util
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年6月24日        下午9:06:23
 */
public class MD5Util {
	
	final static char hexDigits[]={'A','B','C','D','E','F','G','X','Y','Z','0','1','2','3','4','5'};
	
	final static String salt = "BasicManagementSystem"; 
	
	public static String encrypt(String text)
	{
		text =text + salt;
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			byte[] input = text.getBytes();
			md.update(input);
			byte[] out = md.digest();
			int j = out.length;
			char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = out[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];//取高四位
                str[k++] = hexDigits[byte0 & 0xf];//取低四位
            }
            return new String(str);
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
				
		return null;
		
	}
	
	public static void main(String []a)
	{
		System.out.println(MD5Util.encrypt("123456"));
	}
}
