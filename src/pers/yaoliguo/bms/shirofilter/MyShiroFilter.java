package pers.yaoliguo.bms.shirofilter;

import java.io.IOException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.PermissionsAuthorizationFilter;

public class MyShiroFilter extends PermissionsAuthorizationFilter {

	@Override
	public boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue)
			throws IOException {
		boolean permits = true;
		HttpServletRequest req = (HttpServletRequest) request;
		String path = req.getServletPath();
		String regex = ".*skip.*";
		if (path.matches(regex)) {
			Subject currentUser = SecurityUtils.getSubject();
			permits = currentUser.isPermitted(path);
		}
		return permits;
	}

	 

}
