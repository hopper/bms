package pers.yaoliguo.bms.service;

import java.util.List;
import java.util.Map;

import pers.yaoliguo.bms.entity.SysMenu;
import pers.yaoliguo.bms.entity.SysRoleMenuKey;

/**
 * @ClassName:       ISysMenuService
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年6月30日        下午8:56:28
 */
public interface ISysMenuService {
	
	int deleteByPrimaryKey(String id);

    int insert(SysMenu record);

    int insertSelective(SysMenu record);

    SysMenu selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysMenu record);

    int updateByPrimaryKey(SysMenu record);
    
    int selectCount(SysMenu record);
    
    List<SysMenu> selectAll(SysMenu record);
    
    public int removeChildren(SysMenu record);
    
    List<SysMenu> selectByIds(List<SysRoleMenuKey> list);

}
