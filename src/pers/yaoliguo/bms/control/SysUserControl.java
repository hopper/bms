package pers.yaoliguo.bms.control;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pers.yaoliguo.bms.common.PagerBean;
import pers.yaoliguo.bms.control.view.Message;
import pers.yaoliguo.bms.control.view.UserView;
import pers.yaoliguo.bms.entity.SysUser;
import pers.yaoliguo.bms.service.ISysUserService;
import pers.yaoliguo.bms.uitl.MD5Util;
import pers.yaoliguo.bms.uitl.PermissionCodeUtil;
import pers.yaoliguo.bms.uitl.StringHelper;

/**
 * @ClassName: UserControl
 * @Description: TODO
 * @author: yao
 * @date: 2017年6月24日 下午5:10:00
 */
@Controller
@RequestMapping(value = "/UserControl")
public class SysUserControl extends BaseControl {
	@Autowired
	ISysUserService sysUserService;
	private int dataCount;

	//@RequiresPermissions(value = PermissionCodeUtil.USER)
	@RequestMapping("/skipUserPage")
	public String skipMenuRolePage() {
		return "redirect:/views/user/userList.html";
	}
	//@RequiresPermissions(value = PermissionCodeUtil.USER)
	@ModelAttribute("PagerBean")
	public PagerBean setDataCount() {
		System.out.println("Test Pre-Run");
		/**
		 * 设置缺省值
		 */
		PagerBean page = new PagerBean();
		page.setPageNumber(1);
		page.setPageSize(10);
		page.setSc("asc");
		return page;
	}
	 
	//@RequiresPermissions(value = PermissionCodeUtil.USER)
	@RequestMapping(value = "/getUserList")
	@ResponseBody
	public Object getUserList(@ModelAttribute("PagerBean") PagerBean page, UserView userView) {
		Message<SysUser> msg = new Message<SysUser>();
		List<SysUser> users = new ArrayList<SysUser>();
		try {
			dataCount = sysUserService.selectCount(new SysUser(),page);
			page.setRecordCount(dataCount);
			page.setData(dataCount);
			users = sysUserService.selectAllByPage(userView, page);
			msg.setDataList(users);
			msg.setResult("200");
			msg.setPage(page);
		} catch (Exception e) {
			
			msg.setResult("500");
		}
		return msg;
	}
	//@RequiresPermissions(value = PermissionCodeUtil.USER)
	@RequestMapping(value = "/addUser")
	@ResponseBody
	public Object addUser(UserView userView) {
		Message msg = new Message();
		try {
			userView.setPassword(MD5Util.encrypt(userView.getPassword()));
			userView.setCreateTime(new Date());
			userView.setId(StringHelper.getUUID());
			userView.setDel(false);
			sysUserService.insert(userView); 
			msg.setResult("200");
		} catch (Exception e) {
			// TODO: handle exception
			msg.setResult("500");
		}
		return msg;
	}
	//@RequiresPermissions(value = PermissionCodeUtil.USER)
	@RequestMapping(value = "/updateUser",method=RequestMethod.POST)
	@ResponseBody
	public Object updateUser(UserView userView) {
		Message<SysUser> msg = new Message<SysUser>();
		try {
			sysUserService.updateByPrimaryKey(userView);
			msg.setResult("200");
		} catch (Exception e) {
			// TODO: handle exception
			msg.setResult("500");
		}
		return msg;
	}
	//@RequiresPermissions(value = PermissionCodeUtil.USER)
	@RequestMapping(value = "/deleteUser")
	@ResponseBody
	public Object deleteUser(String userId) {
		Message<SysUser> msg = new Message<SysUser>();
		try {
			sysUserService.deleteByPrimaryKey(userId);
			msg.setResult("200");
		} catch (Exception e) {
			// TODO: handle exception
			msg.setResult("500");
		}
		return msg;
	}
}
