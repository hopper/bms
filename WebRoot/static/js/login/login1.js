var login = new Vue({
	el:"#login",
	data:{
		userView:{
				account:"root",
				password:"root",
				code:""
		},
		info:"",
		stop:false,
		postURL:baseUtil.WebRootUrl+"/LoginControl/login/auth",
		codeUrl:"../CodeControl/code?rand="+Math.random(),
		TitleShow:false,
		backgroundColors:[
		        {pos:'bottom',from:'#c79e9e',to:'#5f5757'},
		        {pos:'bottom',from:'rgba(187, 178, 85, 0.78)',to:'#ca6161'},
		        {pos:'bottom',from:'rgba(62, 208, 58, 0.78)',to:'rgba(62, 208, 58, 0.78)'},
		        {pos:'bottom',from:'rgba(208, 58, 129, 0.87)',to:'#795a79'},
		        {pos:'bottom',from:'#c79e9e',to:'#5f5757'},
		        ]
	},
	mounted:function(){
		this.showTitle();
		this.changeBodyBg();
	},
	methods:{
		changeBodyBg:function(){
			 
			 
			
			this.stopFlag(0);
		},
		 stopFlag : function(i){
			 
			   var vm = this;
			  if(!vm.stop){
				  var l = this.backgroundColors.length;
				  var el = document.body;
				  Velocity(el, { opacity:0 }, { 
					  duration: 1000, 
					  complete:function(){
						  document.body.style.background = "linear-gradient(to "+vm.backgroundColors[(i)%l].pos+","+vm.backgroundColors[(i)%l].from+","+vm.backgroundColors[(i)%l].to+")";  
						  Velocity(el, { opacity:1 }, { 
							  duration: 1000 
							  
						  });
					  }
				  });
				  
			  setTimeout(function(){
				  vm.stopFlag(i + 1);  
			  },2500);
			  } 
			},
		 beforeEnter: function (el) {
		      el.style.opacity = 0;
		      el.style.display = 'block';
		     // el.style.transformOrigin = 'left';
		    },
		    enter: function (el, done) {
		      Velocity(el, { opacity: 1,rotateY: "360deg" }, { duration: 1000 });
		      //Velocity(el, { fontSize: '1em' }, { complete: done });
		    },
		    leave: function (el, done) {
		      Velocity(el, { translateX: '15px', rotateZ: '50deg' }, { duration: 600 });
		      Velocity(el, { rotateZ: '100deg' }, { loop: 2 });
		      Velocity(el, {
		        rotateZ: '45deg',
		        translateY: '30px',
		        translateX: '30px',
		        opacity: 0
		      }, { complete: done })
		    },
		showTitle:function(){
			this.TitleShow = true;
		},
		submit:function(){
			vm = this;
			vm.$http.post(this.postURL,this.userView,{emulateJSON:true})
			.then((response) => {
				this.info = response.data.info;
				if(response.data.result == "200"){
					window.location.href = "index.html";
				}
			})
			.catch((response) =>{
				this.info = "有异常了！";
			})
			
		},
		changeCode:function(){
			this.codeUrl = "../CodeControl/code?rand="+Math.random();
		}
	}
});
var head = new Vue({
	el:"#head",
	data:{
		stop:true
	},
	watch:{
		stop:function(val){
			login.stop = !val;
			login.stopFlag(0);
		}
	}
});
if(window.top != window.self){
	top.location.href = location.href;  
}