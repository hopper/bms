package pers.yaoliguo.bms.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import pers.yaoliguo.bms.entity.SysOrganiz;
@Repository("sysOrganizDao")
public interface SysOrganizDao {
    int deleteByPrimaryKey(String id);

    int insert(SysOrganiz record);

    int insertSelective(SysOrganiz record);

    SysOrganiz selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysOrganiz record);

    int updateByPrimaryKey(SysOrganiz record);
    
    List<SysOrganiz>selectAll(Map m);
}