 require(['vue','ELEMENT','base','vue-resource','Message'],
		function(Vue,ELEMENT,baseUtil,VueResource,messageHelper){
	 //安装 vue的ajax功能
    Vue.use(VueResource);
    //安装element
    Vue.use(ELEMENT);
    //安装message
    Vue.use(messageHelper);
    var RoleList = new  Vue({
    	el:"#menuroleList",
    	 watch: {
    	      filterText:function(val) {
    	        this.$refs.bmsrolemanage.filter(val);
    	      }
    	    },
    	data:{ 
    		defaultMenus:[],
    		//存放角色的数据
            roledata: [{pid:'',id:"-1",roleName:"一级角色",icon:"",children:[]},
          		     ],
          	//存放菜单的数据
          	 menudata:[{pid:'',id:"-1",menuName:"一级菜单",icon:"",children:[]}],
            roleProps: {  
                children: 'children',  
                label: 'roleName'  
            }  ,
            menuProps: {  
                children: 'children',  
                label: 'menuName'  
            }, 
            roleFilterText:"",
            menuFilterText:"",
            roleId:'-1',
            getRoleURL:baseUtil.WebRootUrl+"/SysRoleControl/getRoleList",
            getMenuByRoleIdURL:baseUtil.WebRootUrl+"/SysMenuRole/getMenusByRoleId",
            saveMenuRoleURL:baseUtil.WebRootUrl+"/SysMenuRole/saveMenuRole",
            getMenuURL:baseUtil.WebRootUrl+"/SysMenuControl/getMenuList"
    	},
    	watch:{
    		roleFilterText:function(val) {
    	        this.$refs.roleTree.filter(val);
  	      },
  	    menuFilterText:function(val) {
	        this.$refs.menuTree.filter(val);
	      }
    	},
    	mounted:function(){ 
    		this.getRoleData();
    		this.getMenuData();
    	},
    	methods:{ 
    		getMenusByRole:function(obj,data,node){
    			this.getMenuByRoleId(obj.id);
    		},
    		roleFilterNode:function(value, data) {
    	            if (!value) return true;
    	            return data.roleName.indexOf(value) !== -1;
    	          },
    	    menuFilterNode:function(value, data) {
    	              if (!value) return true;
    	              return data.menuName.indexOf(value) !== -1;
    	            },
    	            saveMenuRoleURL:function(){
    	            	
    	            }, 
    	    getRoleData:function(){
    	    	var vm = this;
    			vm.$http.get(vm.getRoleURL,{"pid":"-1"},{emulateJSON:true})
    			.then((response) => {
    				vm.info = response.data.info;
    				if(response.data.result == "200"){
     				 vm.roledata[0].children.length = 0;
    				 vm.removeChilren(response.data.dataList);
    				 vm.$set(vm.roledata[0],'children',response.data.dataList); 	 
     				}
    			})
    			.catch(function(response) {
    				vm.info="有异常了";
    			})
    	    },
    	    getMenuData:function(){
    	    	var vm = this;
    			vm.$http.get(vm.getMenuURL,{"roleId":"-1"},{emulateJSON:true})
    			.then((response) => {
    				vm.info = response.data.info;
    				if(response.data.result == "200"){
     				 vm.menudata[0].children.length = 0;
    				 vm.removeChilren(response.data.dataList);
    				 vm.$set(vm.menudata[0],'children',response.data.dataList); 	 
     				}
    			})
    			.catch(function(response) {
    				vm.info="有异常了";
    			})
    	    },
    	    getMenuByRoleId:function(roleId){
    	    	var vm = this;
    	    	vm.roleId = roleId;
    			vm.$http.get(vm.getMenuByRoleIdURL,{"roleId":roleId},{emulateJSON:true})
    			.then((response) => {
    				vm.info = response.data.info;
    				if(response.data.result == "200"){
     				 vm.defaultMenus.length = 0;
    				 vm.removeChilren(response.data.dataList);
    				 response.data.dataList.forEach(function(item){
    					 vm.defaultMenus.push(item.menuId);
    				 });
    				 vm.$refs.menuTree.setCheckedKeys(vm.defaultMenus);
    				// vm.$set(vm.menudata[0],'children',response.data.dataList); 	 
     				}
    			})
    			.catch(function(response) {
    				vm.info="有异常了";
    			})
    	    },
    	    saveMenuRole:function(){
    	    	var vm = this;
    	    	var menuroleJson = [];
    	    	var getCheckMens = vm.$refs.menuTree.getCheckedKeys();
    	    	getCheckMens.forEach((item)=>(menuroleJson.push({roleId:vm.roleId,menuId:item})));
    	    	getCheckMens.length == 0?menuroleJson.push({roleId:vm.roleId,menuId:'-1'}):"";
    	    	if(vm.roleId == '-1'){
    	    		
    	    		vm.$showMess({message:"请现在左侧选择一个不为一级角色的角色！",messType:'info'}); 
    	    		return;
    	    	}
 				vm.$http.post(vm.saveMenuRoleURL,JSON.stringify(menuroleJson),{emulateJSON:true})
    			.then((response) => {
    				vm.info = response.data.info;
    				if(response.data.result == "200"){
    			     vm.$showMess({message:"保存成功！",messType:'success'}); 
     				 vm.getMenuByRoleId(vm.roleId);
    				 
    				// vm.$set(vm.menudata[0],'children',response.data.dataList); 	 
     				}
    			})
    			.catch(function(response) {
    				vm.info="有异常了";
    				vm.$showMess({message:"保存失败！",messType:'error'});
    			})
    	    },
    	    removeChilren:function(arr){
  			  let i = 0;
  			  for(;i<arr.length;i++){
  				  if(arr[i].children.length > 0){
  					  this.removeChilren(arr[i].children);
  				  }
  				  else{
  					  delete arr[i].children;
  				  }
  			  }
  		},
    	}
    });
});