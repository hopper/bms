package pers.yaoliguo.bms.service;

import java.util.List;
import java.util.Map;

import pers.yaoliguo.bms.entity.SysMenu;
import pers.yaoliguo.bms.entity.SysOrganiz;
import pers.yaoliguo.bms.entity.SysRole;

/**
 * @ClassName:       ISysRoleService
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年7月4日        下午10:32:44
 */
public interface ISysOrganizService {
	
	  int deleteByPrimaryKey(String id);

	    int insert(SysOrganiz record);

	    int insertSelective(SysOrganiz record);

	    SysOrganiz selectByPrimaryKey(String id);

	    int updateByPrimaryKeySelective(SysOrganiz record);

	    int updateByPrimaryKey(SysOrganiz record);
	    
	    List<SysOrganiz>selectAll(SysOrganiz s);
	    
	    public int removeChildren(SysOrganiz record);
	  
	    public List<SysOrganiz> selectOrganizsByKey(SysOrganiz record);
}
